/** @jsx React.DOM */
var Router = require('react-router'),
    Routes = Router.Routes,
    Route = Router.Route,
    Notes = require('./notes'),
    Note = require('./notes/note'),

    AppRoutes;

AppRoutes = React.createClass({
    render: function () {
        return (
            <Routes>
                <Route handler={this.props.app}>
                    <Route name="notes" handler={Notes}>
                        <Route name="note" path="/notes/note/:id" handler={Note.Detail}/>
                        <Route name="note-edit" path="/notes/note/:id/edit" handler={Note.Edit}/>
                    </Route>
                </Route>
            </Routes>
            )
    }
});


module.exports = AppRoutes;