/** @jsx React.DOM */
var React = require('react'),

    Header;

Header = React.createClass({
    render: function () {
        return <header>
            <div className="row clear">
                <h1>Notes application</h1>
            </div>
        </header>
    }
});

module.exports = Header;
