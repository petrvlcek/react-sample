/** @jsx React.DOM */
var React = require('react'),
    $ = require('jquery'),
    Router = require('react-router'),
    Link = Router.Link,

    Repository = require('../repository'),
    Notes;

Notes = React.createClass({
    getInitialState: function () {
        return {
            notes: []
        };
    },
    componentDidMount: function () {
        this.fetchAllNotes();
    },
    fetchAllNotes: function () {
        Repository.NotesRepository.findAll(function (notes) {
            var newState = {
                notes: notes
            };

            this.setState(newState);

        }.bind(this));
    },
    render: function () {
        var notes = this.state.notes.map(function (note) {
            return (
                <li>
                    <Link to="note" id={note.id}>{note.title}</Link>
                </li>
                )
        });

        return  (
            <div className="row clear">
                <div className="col col-4 tablet-col-8 mobile-col-1-2">
                    <ol className="no-tablet no-mobile">
                        {notes}
                    </ol>
                </div>
                <div className="note-detail col col-8 tablet-col-4 mobile-col-1-2">
                    {this.props.activeRouteHandler ? (this.props.activeRouteHandler() || <span>Please select a note</span>) : ''}
                </div>
            </div>
            )

    }
});

module.exports = Notes;
