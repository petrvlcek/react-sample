/** @jsx React.DOM */
var React = require('react'),

    Router = require('react-router'),
    Link = Router.Link,
    Repository = require('../repository'),

    NoteDetail;

NoteDetail = React.createClass({
    getInitialState: function () {
        return {
            note: null
        };
    },
    componentDidMount: function () {
        var id = this.props.params.id;
        Repository.NotesRepository.findById(id, function(note){
            this.setState({
                note: note
            });
        }.bind(this));
    },
    render: function () {
        var note = this.state.note;

        if (note) {
            return (
                <div>
                    <div>
                        {note.content}
                    </div>
                    <div>
                        <br/>
                        <Link to="note-edit" id={note.id}>Edit note</Link>
                    </div>
                </div>
                );
        } else {
            return null;
        }
    }
});

module.exports.Detail = NoteDetail;
module.exports.Edit = NoteDetail;

