var rest = require("rest"),
  mime = require("rest/interceptor/mime"),
  pathPrefix = require("rest/interceptor/pathPrefix"),
  entity = require("rest/interceptor/entity"),

  endpoint = "http://reactsample.apiary-mock.com";

/**
 * Abstract REST repository.
 */
class Repository {

    constructor(resourcePath) {
        this.path = resourcePath;
        this.client = rest
            .wrap(pathPrefix, {
                prefix: endpoint
            })
            .wrap(mime, {
                mime: 'application/json'
            })
            .wrap(entity);
    }

    findAll(success) {
        this.client({
            path: this.path
        }).then(function(response) {
            success(response);
        });
    }

    findById(id, success) {
        this.client({
            path: this.path + '/' + id
        }).then(function(response) {
            success(response);
        });
    }
}

module.exports = Repository;
