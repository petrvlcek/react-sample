var Repository = require('./repository');

class NotesRepository extends Repository {
    constructor() {
        super('notes');
    }
}

module.exports = NotesRepository;