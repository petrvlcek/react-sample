var gulp = require('gulp'),

    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    plumber = require('gulp-plumber'),
    notify = require("gulp-notify"),
    browserSync = require('browser-sync'),
    del = require('del');

var reload = browserSync.reload;

/* Configuration */
var config = require('./config.json');

/* Tasks */
gulp.task('clean', function (cb) {
    del([config.paths.dest.root], cb);
});

gulp.task('clean-lib', function (cb) {
    del([config.paths.lib.node_modules], cb);
    del([config.paths.lib.bower_components], cb);
});

gulp.task('styles', function () {

    gulp.src(config.paths.src.styles)
        .pipe(plumber())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest(config.paths.dest.styles))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('scripts', function () {

    var bundler = browserify(config.paths.src.app)
        .transform('reactify')
        .transform('envify')
        .transform('es6ify')
        .bundle()
        .on('error', notify.onError({
            message: "Error: <%= error.message %>",
            title: "Failed running browserify"
        }))
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(plumber())
        .pipe(gulp.dest(config.paths.dest.scripts))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('images', function () {

    gulp.src(config.paths.src.images)
        .pipe(plumber())
        .pipe(imagemin())
        .pipe(gulp.dest(config.paths.dest.images))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('build', ['styles', 'scripts', 'images']);

gulp.task('watch', function () {
    gulp.watch(config.paths.src.scripts, ['scripts'], reload);
    gulp.watch(config.paths.src.styles, ['styles'], reload);
    gulp.watch(config.paths.src.images, ['images'], reload);
});

gulp.task('browser-sync-server', function () {
    browserSync({
        server: {
            baseDir: __dirname
        },
        port: config.server.port
    });
});

gulp.task('browser-sync', function () {
    browserSync();
});

gulp.task('dev', ['build', 'browser-sync', 'watch']);
gulp.task('start', ['build', 'browser-sync-server', 'watch']);
gulp.task('default', ['start']); // Default task
