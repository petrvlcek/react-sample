# react-sample

Quickstart project template for web application with React.

Before you start you have to install [node.js](http://nodejs.org/). When you are done just run

1. `npm install -g gulp` to install Gulp
2. `npm install -g bower` to install Bower
3. `npm install` to download dependencies
4. `bower install` to download Bower dependencies 

Now you should be up and running. Use `gulp` or `gulp start` to start local server with continuous build and live reload or just `gulp dev` to enable continuous build with live reload but without starting a local server.

## Some interesting libraries, tools and other things used in this sample ##

* [EcmaScript 6 (ES6)](https://people.mozilla.org/~jorendorff/es6-draft.html) - using [traceur-compiler](https://github.com/google/traceur-compiler) and Browserify transform [es6ify](https://github.com/thlorenz/es6ify)
* [Browserify](http://browserify.org/)
* [React router](https://github.com/rpflorence/react-router)
* [RESTful HTTP client for JavaScript](https://github.com/cujojs/rest)
* [BrowserSync](http://www.browsersync.io/) - enables live reload with no need to install any browser plugins, remote live reload, action synchronization, etc.